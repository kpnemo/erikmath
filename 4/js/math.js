var actions = ['+', '-'];

var timerEl = $('.timer time'),
	seconds = 0, minutes = 0, hours = 0,
	t;

function timer(stop) {
	if(stop) {
		clearTimeout(t);
		timerEl.attr('data-status', 'stopped');
	} else {
		t = setTimeout(add, 1000);
		timerEl.attr('data-status', 'started');
	}
}

function add() {
	seconds++;
	if (seconds >= 60) {
		seconds = 0;
		minutes++;
		if (minutes >= 60) {
			minutes = 0;
			hours++;
		}
	}

	timerEl.html((hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds));
	timer();
}

$(function(){

	$('.desatki').on('click', function(ev){
		ev.preventDefault();
		$('.reset').click();
		$('.num').each(function(){
			var n = parseInt($(this).html());
			var nn = n * 10;
			$(this).html(nn);
		});
	});

	$('.reset').on('click', function(ev){
		ev.preventDefault();
		$('.fa').hide();
		$('input').val('');
		$('.task').each(function(){
			var _task = $(this);
			var _input = $(this).find('input');
			var _nums = [];
			var _actions = [];

			var _firstNum = Math.floor(Math.random() * 99) + 1;
			var _action = Math.floor(Math.random() * 2) + 0;
			var _secondNum = null;

			if(_action == 1){ //in case it's minus
				_secondNum = Math.floor(Math.random() * _firstNum) + 1
			} else { //in case it's plus
				_secondNum = Math.floor(Math.random() * (99 - _firstNum)) + 1
			}

			_task.find('.num').eq(0).html(_firstNum);
			_task.find('.action').eq(0).html(actions[_action]);
			_task.find('.num').eq(1).html(_secondNum);

			_task.removeClass('good');

			timer(true);
			timerEl.html("00:00:00");
			seconds = 0; minutes = 0; hours = 0;

		})
	})

	$('.task').each(function(){

		var _task = $(this);
		var _input = $(this).find('input');

		var _firstNum = Math.floor(Math.random() * 99) + 1;
		var _action = Math.floor(Math.random() * 2) + 0;
		var _secondNum = null;

		if(_action == 1){
			_secondNum = Math.floor(Math.random() * _firstNum) + 1
		} else {
			_secondNum = Math.floor(Math.random() * (99 - _firstNum)) + 1
		}

		_task.find('.num').eq(0).html(_firstNum);
		_task.find('.action').eq(0).html(actions[_action]);
		_task.find('.num').eq(1).html(_secondNum);

		_input.on('focus', function(){
			if(timerEl.attr('data-status') != 'started'){
				timer();
			}
		});

		_input.on('keypress', function(event){
			if (event.which != 46 && (event.which < 47 || event.which > 59))
			{
				event.preventDefault();
				if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
					event.preventDefault();
				}
			}
		})

		_input.on('keyup', function(ev){

			var task = null;
			var _nums = [];
			var _actions = [];

			_task.find('.num').each(function(){
				_nums.push(parseInt($(this).html()));
			});
			_task.find('.action').each(function(){
				_actions.push($(this).html());
			});


			if(_nums.length == 2){
				task = _nums[0] + _actions[0] + _nums[1];
			} else if(_nums.length == 3) {
				task = _nums[0] + _actions[0] + _nums[1] + _actions[1] + _nums[2];
			} else if(_nums.length == 4) {
				task = _nums[0] + _actions[0] + _nums[1] + _actions[1] + _nums[2] + _actions[2] + _nums[3];
			}

			var result = eval(task);
			_input.attr('data-result', result);


			var userResult = $(this).val();
			var _result = parseInt(userResult);

			_task.find('.fa').hide();

			if(_result == $(this).attr('data-result')){
				_task.removeClass('error').addClass('good');
				_task.find('.fa-check-circle-o').show();

				var _total = $('.task').length;
				var _good = $('.task.good').length;

				if(_total == _good) {
					timer(true);
				}

			} else {
				_task.find('.fa-frown-o').show();
				_task.removeClass('good').addClass('error');
			}
		});

	});
});
